'''
                                  ESP Health
                         Notifiable Diseases Framework
                     Influenza like illness Case Generator


@author: Carolina Chacin <cchacin@commoninf.com>
@organization: Commonwealth Informatics.
@contact: http://esphealth.org
@copyright: (c) 2012 Commonwealth Informatics
@license: LGPL
'''

from django.db.models import Count
from ESP.utils import log
from ESP.utils.utils import queryset_iterator
from ESP.hef.base import Event, BaseEventHeuristic, LabResultAnyHeuristic
from ESP.hef.base import LabResultPositiveHeuristic, Dx_CodeQuery, HEF_CORE_URI
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import Case
from ESP.conf.models import ConditionConfig
from ESP.static.models import DrugSynonym
from ESP.emr.models import Encounter
from ESP.settings import FILTER_CENTERS
from django.contrib.contenttypes.models import ContentType

FEVER=100

class FeverDiagnosisHeuristic(BaseEventHeuristic):
    '''
    A heuristic for detecting events based on fever diagnosis codes
    from a physician encounter, ensuring that the encounter temperature value is null
    '''
    def __init__(self, name, dx_code_queries):
        '''
        @param name: Name of this heuristic
        @type name:  String
        @param dx_code_queries: Generate event for records matching one of these queries
        @type dx_code_queries:  List of dx codes Query objects
        '''
        assert name and dx_code_queries
        self.name = name
        self.dx_code_queries = dx_code_queries

    def __hash__(self):
        return hash(self.name)

    @property
    def short_name(self):
        sn = u'diagnosis:%s' % self.name
        return sn
    
    uri = u'urn:x-esphealth:heuristic:channing:diagnosis:v1'
    
    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]
    
    def __str__(self):
        return self.short_name
    
    @property
    def encounters(self):
        
        enc_q = self.dx_code_queries[0].encounter_q_obj
        for query in self.dx_code_queries[1:]:
            enc_q |= query.encounter_q_obj
        return Encounter.objects.filter(enc_q)
    
    @property
    def dx_event_name(self):
        return 'dx:%s' % self.name
    
    @property
    def event_names(self):
        return [self.dx_event_name]
    
    def generate(self):
        #get the set of encounter PKs with fever diagnosis
        enc_qs_set = set(self.encounters.values_list('pk',flat=True))
        #get the set of encounter PKs attached to dx:fever events
        excl_qs_set = set(Event.objects.filter(name__in=self.event_names).values_list('object_id',flat=True))
        #get the list of encounter pk and temp values for the fever diagnosis encounters that are not events (yet)
        enc_pks_temp_tups = Encounter.objects.filter(pk__in=enc_qs_set - excl_qs_set).values_list('pk','temperature')
        #go through the list of tuples and if there is no temperature, then keep the pk.
        evnt_pks = set()
        for pk_tup in enc_pks_temp_tups:
             if not pk_tup[1]:
                 evnt_pks.add(pk_tup[0])
        #now get the set of encounter events for those pks, which should be new events.
        enc_qs = Encounter.objects.filter(pk__in=evnt_pks).order_by('date')
        log.info('Generating events for "%s"' % self)
        #log_query('Encounters for %s' % self, enc_qs)
        counter = 0
        for enc in queryset_iterator(enc_qs):
            Event.create(
                name = self.dx_event_name,
                source = self.uri,
                patient = enc.patient,
                date = enc.date,
                provider = enc.provider,
                emr_record = enc,
                )
            counter += 1
        log.info('Generated %s new events for %s' % (counter, self))
        return counter

class EncounterFeverHeuristic(BaseEventHeuristic):
    '''
    Fever Event Heuristic
    '''

    def __init__(self, name):
        '''
        @param name: Name of this heuristic
        @type name:  String
        '''
        assert name
        self.name = name

    def __hash__(self):
        return hash(self.name)

    @property
    def short_name(self):
        return u'enc:%s' % self.name

    uri = u'urn:x-esphealth:heuristic:channing:encbp:v1'
    
    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]

    @property
    def encounters(self):
        fev_enc_pk_lst = Encounter.objects.filter(temperature__gte = FEVER).values_list('pk',flat=True)
        fev_evnt_objid_lst = Event.objects.filter(name='enc:fever').values_list('object_id',flat=True)
        pk_lst = list(set(fev_enc_pk_lst)-set(fev_evnt_objid_lst))
        return Encounter.objects.filter(pk__in=pk_lst)

    @property
    def enc_event_name(self):
        return 'enc:%s' % self.name 

    @property
    def event_names(self):
        return [self.enc_event_name]

    def generate(self):
        log.info('Generating events for "%s"' % self)
        counter = 0
        for enc in queryset_iterator(self.encounters):
            Event.create(name = self.enc_event_name,
                    source=self.uri,
                    patient = enc.patient,
                    date = enc.date,
                    provider = enc.provider,
                    emr_record = enc,
                    )
            counter += 1

        log.info('Generated %s new events for %s' % (counter, self))
        return counter


class DiagnosisHeuristic(BaseEventHeuristic):
    '''
    A heuristic for detecting events based on one or more diagnosis codes
    from a physician encounter.
    '''
    def __init__(self, name, dx_code_queries):
        '''
        @param name: Name of this heuristic
        @type name:  String
        @param dx_code_queries: Generate event for records matching one of these queries
        @type dx_code_queries:  List of dx codes Query objects
        '''
        assert name and dx_code_queries
        self.name = name
        self.dx_code_queries = dx_code_queries

    def __hash__(self):
        return hash(self.name)

    @property
    def short_name(self):
        sn = u'diagnosis:%s' % self.name
        return sn
    
    uri = u'urn:x-esphealth:heuristic:channing:diagnosis:v1'
    
    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]
    
    def __str__(self):
        return self.short_name
    
    @property
    def encounters(self):
        #local mod to just return set of enc pks
        enc_q = self.dx_code_queries[0].encounter_q_obj
        for query in self.dx_code_queries[1:]:
            enc_q |= query.encounter_q_obj
        return set(Encounter.objects.filter(enc_q).values_list('pk',flat=True))
    
    @property
    def dx_event_name(self):
        return 'dx:%s' % self.name
    
    @property
    def event_names(self):
        return [self.dx_event_name]
    
    def generate(self):
        enc_pkset = self.encounters
        pk_lst = list(enc_pkset - set(Event.objects.filter(name__in=self.event_names).values_list('object_id',flat=True)))
        enc_qs = Encounter.objects.filter(pk__in=pk_lst).values('pk','patient','date','provider')
        log.info('Generating events for "%s"' % self)
        #log_query('Encounters for %s' % self, enc_qs)
        counter = 0
        content_type = ContentType.objects.get_for_model(Encounter)
        for enc in enc_qs:
            e = Event(
                name = self.dx_event_name,
                source = self.uri,
                date = enc['date'],
                patient_id = enc['patient'],
                provider_id = enc['provider'],
                content_type = content_type,
                object_id = enc['pk'],
                )
            e.save()
            counter += 1
        log.info('Generated %s new events for %s' % (counter, self))
        return counter

class ili(DiseaseDefinition):
    '''
    Influenza like illness
    '''
    
    conditions = ['ili']
    
    uri = 'urn:x-esphealth:disease:commoninf:ili:v1'
    
    short_name = 'ili'
    
    # no tests for ili
    test_name_search_strings = [  ]
    
    timespan_heuristics = []
    
    recurrence_interval = 42
    
    @property
    def event_heuristics(self):
        heuristic_list = []
        
        #
        # High blood pressure
        #
        heuristic_list.append( EncounterFeverHeuristic(
            name = 'fever'
            ))        #
        # Diagnosis Codes for fever
        #  
        heuristic_list.append( DiagnosisHeuristic(
            name = 'fever',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='780.6', type='icd9'),
                 Dx_CodeQuery(starts_with='780.31', type='icd9'),
                 Dx_CodeQuery(starts_with='R50.2', type='icd10'),
                 Dx_CodeQuery(starts_with='R50.8', type='icd10'),
                 Dx_CodeQuery(starts_with='R50.84', type='icd10'),
                 Dx_CodeQuery(starts_with='R50.9', type='icd10'),
                 Dx_CodeQuery(starts_with='R56.00',type='icd10'),
                ]))
        
        #
        # Diagnosis Codes for ili
        #        
        heuristic_list.append( DiagnosisHeuristic(
            name = 'ili',
            dx_code_queries = [

                 Dx_CodeQuery(starts_with='079.3', type='icd9'),
                 Dx_CodeQuery(starts_with='079.89', type='icd9'),
                 Dx_CodeQuery(starts_with='079.99', type='icd9'),
                 Dx_CodeQuery(starts_with='460', type='icd9'),
                 Dx_CodeQuery(starts_with='462', type='icd9'),
                 Dx_CodeQuery(starts_with='464.00', type='icd9'),
                 Dx_CodeQuery(starts_with='464.01', type='icd9'),
                 Dx_CodeQuery(starts_with='464.10', type='icd9'),
                 Dx_CodeQuery(starts_with='464.11', type='icd9'),
                 Dx_CodeQuery(starts_with='464.20', type='icd9'),
                 Dx_CodeQuery(starts_with='464.21', type='icd9'),
                 Dx_CodeQuery(starts_with='465.0', type='icd9'),
                 Dx_CodeQuery(starts_with='465.8', type='icd9'),
                 Dx_CodeQuery(starts_with='465.9', type='icd9'),
                 Dx_CodeQuery(starts_with='466.0', type='icd9'),
                 Dx_CodeQuery(starts_with='466.19', type='icd9'),
                 Dx_CodeQuery(starts_with='478.9', type='icd9'),
                 Dx_CodeQuery(starts_with='480.8', type='icd9'),
                 Dx_CodeQuery(starts_with='480.9', type='icd9'),
                 Dx_CodeQuery(starts_with='481', type='icd9'),
                 Dx_CodeQuery(starts_with='482.40', type='icd9'),
                 Dx_CodeQuery(starts_with='482.41', type='icd9'),
                 Dx_CodeQuery(starts_with='482.42', type='icd9'),
                 Dx_CodeQuery(starts_with='482.49', type='icd9'),
                 Dx_CodeQuery(starts_with='484.8', type='icd9'),
                 Dx_CodeQuery(starts_with='485', type='icd9'),
                 Dx_CodeQuery(starts_with='486', type='icd9'),
                 Dx_CodeQuery(starts_with='487.0', type='icd9'),
                 Dx_CodeQuery(starts_with='487.1', type='icd9'),
                 Dx_CodeQuery(starts_with='487.8', type='icd9'),
                 Dx_CodeQuery(starts_with='784.1', type='icd9'),
                 Dx_CodeQuery(starts_with='786.2', type='icd9'),   
                 Dx_CodeQuery(starts_with='B97.89', type='icd10'),
                 Dx_CodeQuery(starts_with='B33.8', type='icd10'),
                 Dx_CodeQuery(starts_with='B34.1', type='icd10'),
                 Dx_CodeQuery(starts_with='B34.2', type='icd10'),
                 Dx_CodeQuery(starts_with='B34.4', type='icd10'),
                 Dx_CodeQuery(starts_with='B34.8', type='icd10'),
                 Dx_CodeQuery(starts_with='B97.19', type='icd10'),
                 Dx_CodeQuery(starts_with='B97.29', type='icd10'),
                 Dx_CodeQuery(starts_with='B97.89', type='icd10'),
                 Dx_CodeQuery(starts_with='J00', type='icd10'),
                 Dx_CodeQuery(starts_with='J02.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J04.0', type='icd10'),
                 Dx_CodeQuery(starts_with='J05.0', type='icd10'),
                 Dx_CodeQuery(starts_with='J04.10', type='icd10'),
                 Dx_CodeQuery(starts_with='J04.11', type='icd10'),
                 Dx_CodeQuery(starts_with='J04.2', type='icd10'),
                 Dx_CodeQuery(starts_with='J05.0', type='icd10'),
                 Dx_CodeQuery(starts_with='J06.0', type='icd10'),
                 Dx_CodeQuery(starts_with='J06.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J20.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J21.8', type='icd10'),
                 Dx_CodeQuery(starts_with='J21.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J39.8', type='icd10'),
                 Dx_CodeQuery(starts_with='J39.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J12.89', type='icd10'),
                 Dx_CodeQuery(starts_with='J12.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J13', type='icd10'),
                 Dx_CodeQuery(starts_with='J18.1', type='icd10'),
                 Dx_CodeQuery(starts_with='J15.20', type='icd10'),
                 Dx_CodeQuery(starts_with='J15.211', type='icd10'),
                 Dx_CodeQuery(starts_with='J15.212', type='icd10'),
                 Dx_CodeQuery(starts_with='J15.29', type='icd10'),
                 Dx_CodeQuery(starts_with='J17', type='icd10'),
                 Dx_CodeQuery(starts_with='J18.0', type='icd10'),
                 Dx_CodeQuery(starts_with='J18.8', type='icd10'),
                 Dx_CodeQuery(starts_with='J18.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J10.00', type='icd10'),
                 Dx_CodeQuery(starts_with='J10.08', type='icd10'),
                 Dx_CodeQuery(starts_with='J11.00', type='icd10'),
                 Dx_CodeQuery(starts_with='J11.08', type='icd10'),
                 Dx_CodeQuery(starts_with='J12.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J10.01', type='icd10'),
                 Dx_CodeQuery(starts_with='J10.1', type='icd10'),
                 Dx_CodeQuery(starts_with='J11.1', type='icd10'),
                 Dx_CodeQuery(starts_with='J10.2', type='icd10'),
                 Dx_CodeQuery(starts_with='J10.81', type='icd10'),
                 Dx_CodeQuery(starts_with='J10.82', type='icd10'),
                 Dx_CodeQuery(starts_with='J10.83', type='icd10'),
                 Dx_CodeQuery(starts_with='J10.89', type='icd10'),
                 Dx_CodeQuery(starts_with='J11.2', type='icd10'),
                 Dx_CodeQuery(starts_with='J11.81', type='icd10'),
                 Dx_CodeQuery(starts_with='J11.82', type='icd10'),
                 Dx_CodeQuery(starts_with='J11.83', type='icd10'),
                 Dx_CodeQuery(starts_with='J11.89', type='icd10'),
                 Dx_CodeQuery(starts_with='R07.0', type='icd10'),
                 Dx_CodeQuery(starts_with='R05', type='icd10'),
                ]
            ))
        
        
        return heuristic_list

    def add_events_to_existing_cases(self,patients,events): 
        #I'm createing this locally because the nodis/base version takes events and creates a full event dict, which uses more memory
        case_patients = set()
        existing_cases = Case.objects.filter(
            patient__in = patients,
            condition = self.conditions[0],
            )
        for case in existing_cases:
            case_patients.add(case.patient)
            self._update_case_from_event_list(case,
                            relevant_events = events.get(case.patient.id, []))
        return case_patients
    
    def create_case_from_event_qs(self, 
            condition,
            criteria,
            recurrence_interval,
            event_qs, 
            relevant_event_names = [],):
                   
        counter = 0
        #log_query('Events for %s' % self.short_name, event_qs)
        for this_event in event_qs:
            created, this_case = self._create_case_from_event_obj(
                condition = condition, 
                criteria = criteria, 
                recurrence_interval = recurrence_interval, 
                event_obj = this_event, 
                relevant_event_names = relevant_event_names,
                )
            if created:
                counter += 1
        return counter
            
    def generate(self):
        log.info('Generating cases of %s' % self.short_name)       
        
        dx_ev_names = ['dx:ili',]
        dx_fever_ev_names = ['dx:fever',]
        enc_fever_ev_names = ['enc:fever',]
 
        # adding cases for each criterion separately because django generates 
        # bad sql when you use '|' to combine query sets.
        #
        # Criteria Set #b 
        # diagnosis of ili and no temperature measured but diagnosis of fever 
        #
       
        #events already attached to cases
        case_evnt_pks = set(Case.objects.filter(condition=self.conditions[0]).values_list('events__pk',flat=True))
        #new events
        dx_ili_pk = set(Event.objects.filter(name__in = dx_ev_names).values_list('pk',flat=True)) - case_evnt_pks
        dx_fev_pk = set(Event.objects.filter(name__in = dx_fever_ev_names).values_list('pk',flat=True)) - case_evnt_pks
        enc_fev_pk = set(Event.objects.filter(name__in = enc_fever_ev_names).values_list('pk',flat=True)) - case_evnt_pks
        ili_evnt_pks = dx_ili_pk | dx_fev_pk | enc_fev_pk
        
        #now see if any of these events can attach to an existing case
        ili_event_qs = Event.objects.filter(pk__in = ili_evnt_pks ).values('patient','pk','date','provider')
        
        #build a dictionary of patients and events
        event_lists_dict = {}
        for event in ili_event_qs:
            try:
                event_lists_dict[event['patient']].append((event['pk'],event['date'],event['provider']))
            except KeyError:
                event_lists_dict[event['patient']] = [(event['pk'],event['date'],event['provider'])]
        event_pats = set(event_lists_dict.keys())
        #build a dictionary of patients and cases
        case_lists_qs = Case.objects.filter(condition=self.conditions[0]).values('patient','pk','date')
        case_lists_dict = {}
        for case in case_lists_qs:
            try:
                case_lists_dict[case['patient']].append((case['pk'],case['date']))
            except KeyError:
                case_lists_dict[case['patient']] =[(case['pk'],case['date'])]
        case_pats = set(case_lists_dict.keys())
        #now go through the events, 1 patient at a time
        added_evnts = set()
        for pat in event_pats:
            #sort events by date
            pat_events = event_lists_dict[pat]
            for event in pat_events:
                #check to see if event can be attached to an existing case:
                try:
                    case_list = case_lists_dict[pat]
                    if len(case_list) > 1:
                        case_list.sort(key=lambda x: x[1])
                    for case in case_list:
                        if abs((event[1]-case[1]).days) < self.recurrence_interval:
                            case_obj = Case.objects.get(pk=case[0])
                            case_obj.events.add(event[0])
                            case_obj.save()
                            #add the event to the case events set
                            added_evnts.add(event[0])
                            break
                except KeyError:
                    #no cases for this patient, so pass
                    pass

        #now pull the remaining events based on their PKs.  
        #this already excludes any events previously or just attached to a case
        dx_ili_pk_reduced = dx_ili_pk - added_evnts
        dx_ili_dicts = Event.objects.filter(pk__in = dx_ili_pk_reduced).values('pk','object_id')
        dx_fev_pk_reduced = dx_fev_pk - added_evnts
        dx_fev_dicts = Event.objects.filter(pk__in = dx_fev_pk_reduced).values('pk','object_id')
        enc_fev_pk_reduced = enc_fev_pk - added_evnts
        enc_fev_dicts = Event.objects.filter(pk__in = enc_fev_pk_reduced).values('pk','object_id')
        
        #this is the set of encounter IDs with encounter fever events
        enc_ids_fev_meas = set([fev_dict['object_id'] for fev_dict in enc_fev_dicts])
        #this is the set of encounter IDs with dx fever events
        enc_ids_fev_dx = set([fev_dict['object_id'] for fev_dict in dx_fev_dicts])
        #this is the set of encounter IDs with dx ili events
        enc_ids_ili_dx = set([ili_dict['object_id'] for ili_dict in dx_ili_dicts])
        #here is the set of encounter IDs with ili dx and fev dx, but no fev enc. 
        dx_ili_dx_fev_intersect = (enc_ids_ili_dx & enc_ids_fev_dx) - enc_ids_fev_meas
        #here is the set of encounter IDs with ili dx and fev enc
        dx_ili_enc_fev_intersect = enc_ids_ili_dx & enc_ids_fev_meas

        #now union the PK dict lists and filter for those matching the relevant encounter IDs
        # we just keep the event IDs (pk) here
        crit1_pks = set()
        dx_ili_dicts_list = list(dx_ili_dicts)
        for pkid_dict in (list(enc_fev_dicts) + dx_ili_dicts_list):
            if pkid_dict['object_id'] in dx_ili_enc_fev_intersect:
                crit1_pks.add(pkid_dict['pk'])
        crit2_pks = set()
        for pkid_dict in (list(dx_fev_dicts) + dx_ili_dicts_list):
            if pkid_dict['object_id'] in dx_ili_dx_fev_intersect:
                crit2_pks.add(pkid_dict['pk'])

        #get the events for criteria 1 cases
        crit1_evnt_qs = Event.objects.filter(pk__in=crit1_pks).values('pk','patient','provider','date')
        event_lists_dict = {}
        for event in crit1_evnt_qs:
            try:
                event_lists_dict[event['patient']].append((event['pk'],event['date'],event['provider'],1))
            except KeyError:
                event_lists_dict[event['patient']] = [(event['pk'],event['date'],event['provider'],1)]
        crit2_evnt_qs = Event.objects.filter(pk__in=crit2_pks).values('pk','patient','provider','date')
        for event in crit2_evnt_qs:
            try:
                event_lists_dict[event['patient']].append((event['pk'],event['date'],event['provider'],2))
            except KeyError:
                event_lists_dict[event['patient']] = [(event['pk'],event['date'],event['provider'],2)]
        event_pats = set(event_lists_dict.keys())
        #now make the cases
        counter = 0
        crit1 = 'Criteria #1: dx:ili and measured temp >= 100'
        crit2 = 'Criteria #2: dx:ili and dx:fever'
        for pat in event_pats:
            if ( FILTER_CENTERS[0]=='' or (FILTER_CENTERS and Patient.objects.value_list('center_id', flat=True).get(pk=pat) in FILTER_CENTERS )):
                status=ConditionConfig.objects.get(name=self.conditions[0]).initial_status
            else:
                status='AR'
            case_date = None
            event_list = event_lists_dict[pat]
            if len(event_list) > 1:
                event_list.sort(key=lambda x: x[1])
            else:
                log.info('Less than 2 events for patient %s' % pat)
            for event in event_list:
                if (not case_date) or ((event[1] - case_date).days > self.recurrence_interval):
                    case_date = event[1]
                    new_case = Case(patient_id=pat, 
                                    provider_id=event[2], 
                                    date=case_date, 
                                    condition=self.conditions[0],
                                    criteria= crit1 if event[3]==1 else crit2,
                                    source=self.uri,
                                    status=status )
                    new_case.save()
                    counter += 1
                new_case.events.add(event[0])
                new_case.save()
                
        log.debug('Generated %s new cases of ili' % counter) 
        return counter # Count of new cases
            
#-------------------------------------------------------------------------------
#
# Packaging
#
#-------------------------------------------------------------------------------

ili_definition = ili()

def event_heuristics():
    return ili_definition.event_heuristics

def disease_definitions():
    return [ili_definition]
